// Function to return object for increment and decrement

function counterFactory(){

    // This is counter
    let counter = 0;

    return {
        increment: () => counter += 1,
        decrement: () => counter -= 1
    };
}

// Export the function
export {counterFactory};