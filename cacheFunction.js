// Function to implement cache function
function cacheFunction(cb) {

    // Map to keep track of arguments
    let cacheMap = new Map();

    return function (...args){

        // If it these set of arguments has passed 
        // before than return cache results
        let key = JSON.stringify(args)

        if(cacheMap.has(key)){

            console.log("Cached result..")
            return cacheMap.get(key);

        } else {

            // Execute the cb function
            let ans = cb(args);

            // Store the ans in cache
            cacheMap.set(key, ans);

            return ans;
        }
    };
}

// Export the function
export {cacheFunction};