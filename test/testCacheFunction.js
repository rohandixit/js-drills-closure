import { cacheFunction } from "../cacheFunction.js";

// Test Funtion to use
function add(elements) {
    let ans = 0;

    for (let element of elements) {
        ans += element;
    }

    return ans
}

// Test the function cacheFunction
let resultFunc = cacheFunction(add);

// Print the results
console.log(resultFunc(1, 2, 3))
console.log(resultFunc(2, 4, 6))
console.log(resultFunc(10, 5))
console.log(resultFunc(1, 2, 3))
console.log(resultFunc(2, 4, 6))