import { limitFunctionCallCount } from "../limitFunctionCallCount.js";

// test variable
let testCounter = 0;

// Test function to be executed
let cb = _ => testCounter += 1;

// Test the function
let resultFunc = limitFunctionCallCount(cb, 3);

// print the results
console.log(resultFunc())
console.log(resultFunc())
console.log(resultFunc())
console.log(resultFunc())
