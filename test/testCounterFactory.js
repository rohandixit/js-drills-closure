import { counterFactory } from "../counterFactory.js";

// Test the function
let obj = counterFactory()

// print the results
console.log(obj.increment())
console.log(obj.increment())
console.log(obj.increment())
console.log(obj.decrement())
console.log(obj.decrement())