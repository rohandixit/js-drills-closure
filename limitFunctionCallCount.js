// Limit Function call count function
function limitFunctionCallCount(cb, n) {

    // Counter used to keep track of count
    let callCounter = 0;

    return () => {

        // Increase the count value by one
        callCounter += 1;

        // if count is less than 1 execute the function
        if(callCounter <= n) {
            return cb();
        } else {
            return null;
        }
    };
}

// export the function
export {limitFunctionCallCount};